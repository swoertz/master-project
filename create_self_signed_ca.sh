#!/bin/bash

#create 2048 bit RSA private key (unencrypted)
openssl genrsa -out ca.key 2048

# create self-signed CA certificate
openssl req -x509 -sha256 -new -days 365 -key ca.key -extensions v3_ca \
    -subj '/C=AT/ST=Tyrol/O=CA/CN=CA' -out ca.pem

