#!/bin/bash

# install dependencies
pacman -S base-devel git cmake botan boost openssl

# compile libtins from source, since it is not available
# as binary package in the Arch Linux repositories
git clone -b v3.5 https://github.com/mfontanini/libtins.git
cd libtins
mkdir build
cd build
cmake ..
make
make install
cd ../..

# compile master-project
git clone https://bitbucket.com/swoertz/master-project.git
cd master-project
mkdir build
cd build
cmake ..
make

