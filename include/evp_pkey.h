/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_EVP_PKEY_H_
#define MASTER_PROJECT_EVP_PKEY_H_

#include <openssl/evp.h>

#include <string>

namespace mp {
class EVP_PKEY {
 public:
    explicit EVP_PKEY(int bits);
    explicit EVP_PKEY(const std::string &keyfile);
    EVP_PKEY(const EVP_PKEY &that);
    EVP_PKEY(EVP_PKEY &&that) noexcept;
    ~EVP_PKEY() noexcept;

    EVP_PKEY &operator=(const EVP_PKEY &that);
    EVP_PKEY &operator=(EVP_PKEY &&that) noexcept;

    void CheckInternalPointerAndRaiseException();
    void PemWrite();
    ::EVP_PKEY *NativeHandle();

 private:
    ::EVP_PKEY *ptr_;
};
}  // namespace mp

#endif  // MASTER_PROJECT_EVP_PKEY_H_
