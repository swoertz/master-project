/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_STREAM_UTIL_H_
#define MASTER_PROJECT_STREAM_UTIL_H_

#include <tins/tcp_ip/stream.h>
#include <tins/tcp_ip/stream_identifier.h>

namespace mp {
//TODO replace with function already merged into StreamIdentifier,
// but not yet released
Tins::TCPIP::StreamIdentifier MakeStreamIdentifier(
    const Tins::TCPIP::Stream &stream) {
  if (stream.is_v6()) {
    return Tins::TCPIP::StreamIdentifier(
      Tins::TCPIP::StreamIdentifier::serialize(stream.client_addr_v6())
      , stream.client_port()
      , Tins::TCPIP::StreamIdentifier::serialize(stream.server_addr_v6())
      , stream.server_port());
  } else {
    return Tins::TCPIP::StreamIdentifier(
      Tins::TCPIP::StreamIdentifier::serialize(stream.client_addr_v4())
      , stream.client_port()
      , Tins::TCPIP::StreamIdentifier::serialize(stream.server_addr_v4())
      , stream.server_port());
  }
}
}  // namespace mp
#endif  // MASTER_PROJECT_STREAM_UTIL_H_

