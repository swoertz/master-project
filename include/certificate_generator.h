/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_CERTIFICATE_GENERATOR_H_
#define MASTER_PROJECT_CERTIFICATE_GENERATOR_H_

#include <openssl/x509.h>

#include <map>
#include <string>

#include "big_num.h"
#include "cache.h"
#include "evp_pkey.h"
#include "x509.h"

namespace mp {
class CertificateGenerator {
 public:
    CertificateGenerator(const std::string &keyFile
        , const std::string &certFile = ""
        , const std::string &serialFile = "");
    ~CertificateGenerator();
    X509 GenerateCertificate(const std::string &subjectCN);
    X509 GetCertificate(const std::string &subjectCN);
    EVP_PKEY GetKey(const std::string *subjectCN = nullptr);
    bool StoreSerialInFile();
 private:
    void ExtractIssuerName(const std::string &filename);
    bool ReadSerialFromFile();
    void SetIssuer(X509 *cert);
    void SetSubject(X509 *cert, const std::string &subjectCN);
    void SetValidity(X509 *cert);
    void SetSerial(X509 *cert);
    std::string Wildcard(const std::string &name);

    Cache cache_;
    EVP_PKEY caKey_;
    ::X509_NAME* issuer_;
    EVP_PKEY key_;
    std::map<std::string, X509> map_;
    std::string serial_file_;
    BigNum serial_;
};
}  // namespace mp

#endif  // MASTER_PROJECT_CERTIFICATE_GENERATOR_H_
