/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_TLS_RECORD_FACTORY_H_
#define MASTER_PROJECT_TLS_RECORD_FACTORY_H_

#include "application_data.h"
#include "client_hello.h"
#include "handshake.h"
#include "server_hello.h"

namespace mp {
class TlsRecordFactory {
  public:
  std::unique_ptr<TlsRecord> create(std::vector<uint8_t> data) {
    TlsRecord tls(data);
    switch(tls.content_type()) {
      case HANDSHAKE: {
        auto h_it = std::next(data.begin(), sizeof(TlsRecordHeader));
        HandshakeType htype = static_cast<HandshakeType>(*h_it);
        if(htype == SERVER_HELLO) {
          return std::make_unique<ServerHello>(data);
        } else if(htype == CLIENT_HELLO) {
          return std::make_unique<ClientHello>(data);
        }
        return std::make_unique<Handshake>(data);
      }
      case APPLICATION_DATA:
        return std::make_unique<ApplicationData>(data);
      case CHANGE_CIPHER_SPEC:
        break;
      case ALERT:
        break;
    }
    auto ptr = std::make_unique<TlsRecord>(data);
    return ptr;
  }
};
}  // namespace mp
#endif // MASTER_PROJECT_TLS_RECORD_FACTORY_H_
