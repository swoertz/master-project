/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_BIG_NUM_H_
#define MASTER_PROJECT_BIG_NUM_H_

#include <openssl/bn.h>

#include <string>

namespace mp {
class BigNum {
 public:
    BigNum();
    BigNum(const BigNum& other);
    BigNum(BigNum&& other) noexcept;
    ~BigNum() noexcept;

    BigNum& operator=(const BigNum& other);
    BigNum& operator=(BigNum&& other) noexcept;

    void CheckInternalPointerAndRaiseException();
    int Increment();
    ::BIGNUM* NativeHandle();
    int SetHex(const std::string &hexstring);
    int SetRandom();
    int SetWord(const unsigned long word);
    std::string ToDec();
    std::string ToHex();

 private:
    BIGNUM* ptr_;
};

}  // namespace mp

#endif  // MASTER_PROJECT_BIG_NUM_H_
