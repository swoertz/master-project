/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_SECURITY_PARAMETERS_H_
#define MASTER_PROJECT_SECURITY_PARAMETERS_H_

#include <array>
#include <cstdint>
#include <string>

namespace mp {
struct SecurityParameters{
  // ConnectionEnd entity;
  // PRFAlgorithm prf_algorithm;
  // BulkCipherAlgorithm bulk_cipher_algorithm;
  // CipherType cipher_type;
  uint8_t enc_key_length;
  uint8_t block_length;
  uint8_t fixed_iv_length;
  uint8_t record_iv_length;
  // MACAlgorithm mac_algorithm;
  uint8_t mac_length;
  uint8_t mac_key_length;
  // CompressionMethod compression_algorithm;
  std::array<uint8_t, 48> master_secret;
  std::array<uint8_t, 32> client_random;
  std::array<uint8_t, 32> server_random;
  std::string client_ip;
  uint16_t  client_port;
};

const SecurityParameters AES_256_SHA{32,16,0,16,20,20,{},{},{},{},{}};
}  // namespace mp

#endif // MASTER_PROJECT_SECURITY_PARAMETERS_H_

