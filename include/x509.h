/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_X509_H_
#define MASTER_PROJECT_X509_H_

#include <cstdint>
#include <string>

#include <openssl/x509v3.h>

#include "big_num.h"
#include "evp_pkey.h"

namespace mp {
class X509 {
 public:
    X509();
    X509(const X509 &that);
    X509(X509 &&that) noexcept;
    explicit X509(const std::string &certificateFile);
    ~X509() noexcept;

    X509 &operator=(const X509 &that);
    X509 &operator=(X509 &&that) noexcept;

    void CheckInternalPointerAndRaiseException();
    int PemWrite(FILE *fp) const;
    int PrintFp(FILE *fp) const;
    int SetVersion(int64_t version);
    int SetPubKey(EVP_PKEY pkey);
    int SetPubKey(::EVP_PKEY *pkey);
    int SetSerialNumber(int64_t serial);
    int SetSerialNumber(BigNum *serial);
    ASN1_INTEGER* GetSerialNumber() const;
    ASN1_TIME* GmtimeAdjustNotBefore(int64_t adj);
    ASN1_TIME* GmtimeAdjustNotAfter(int64_t adj);
    int IssuerAddEntryByTxt(const char *field, const unsigned char *bytes);
    int SubjectAddEntryByTxt(const char *field, const unsigned char *bytes);
    int SetIssuerName(X509_NAME *name);
    int Sign(EVP_PKEY pkey, const EVP_MD *md);
    ::X509* NativeHandle();
    ::X509_NAME* GetSubjectName() const;
    ::X509_NAME* GetIssuerName() const;

 private:
    ::X509 *ptr_;
};
}  // namespace mp

#endif  // MASTER_PROJECT_X509_H_
