/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_APPLICATION_DATA_H_
#define MASTER_PROJECT_APPLICATION_DATA_H_

#include <string>
#include <vector>

#include "tls_record.h"

namespace mp{
class ApplicationData : public TlsRecord {
 public:
  explicit ApplicationData(std::vector<uint8_t> &data);

  std::string Serialize() const override; 
  std::vector<uint8_t> data() const;

 private:
  std::vector<uint8_t> data_;
};
}  // namespace mp
#endif // MASTER_PROJECT_APPLICATION_DATA_H_

