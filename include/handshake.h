/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_HANDSHAKE_H_
#define MASTER_PROJECT_HANDSHAKE_H_

#include <tins/tins.h>
#include <cstdint>
#include <ostream>
#include "protocol_version.h"
#include "tls_record.h"

namespace mp {
enum HandshakeType{
  HELLO_REQUEST = 0,
  CLIENT_HELLO = 1,
  SERVER_HELLO = 2,
  CERTIFICATE = 11,
  SERVER_KEY_EXCHANGE  = 12,
  CERTIFICATE_REQUEST = 13,
  SERVER_HELLO_DONE = 14,
  CERTIFICATE_VERIFY = 15,
  CLIENT_KEY_EXCHANGE = 16,
  FINISHED = 20
};

TINS_BEGIN_PACK
struct Random {
  uint32_t gmt_unix_time;
  uint8_t random_bytes[28];
} TINS_END_PACK;

using SessionId = uint8_t[32];
using CipherSuite = uint8_t[2];
using CompressionMethod = uint8_t;

TINS_BEGIN_PACK
struct HandshakeHeader{
  uint8_t msg_type;
  uint8_t length[3];
} TINS_END_PACK;

class Handshake : public TlsRecord {
  public:
    explicit Handshake(std::vector<uint8_t> &data);
    virtual ~Handshake() {};
    friend std::ostream& operator<<(std::ostream &stream, const Handshake &handshake);
    HandshakeType msg_type() const {return static_cast<HandshakeType>(header_.msg_type);}
    uint32_t length() const; 
    std::string Serialize() const override; 

  private:
    HandshakeHeader header_;
  protected:
    std::vector<uint8_t>::iterator body_;
};
}  // namespace mp
#endif // MASTER_PROJECT_HANDSHAKE_H_

