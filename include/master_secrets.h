/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_MASTER_SECRETS_H_
#define MASTER_PROJECT_MASTER_SECRETS_H_

#include <array> 
#include <map> 
#include <string> 

#include <botan/botan.h> 

#include "security_parameters.h"

namespace mp {
class MasterSecrets {
 public:
   MasterSecrets();
   void Add(const std::string &ip_address, const uint16_t port 
      , const Botan::secure_vector<Botan::byte> &master_key);
   void ClientRandom(const std::string &ip_address, const uint16_t port 
      , const std::array<uint8_t, 32> &client_random);
   void ServerRandom(const std::string &ip_address, const uint16_t port 
      , const std::array<uint8_t, 32> &server_random);
   SecurityParameters* Get(const std::string &src_ip
      , const uint16_t src_port
      , const std::string &dst_ip = ""
      , const uint16_t dst_port = 0); 

 private:
   using id_type = std::string; 

   id_type Id(const std::string &ip_address, const uint16_t port);
   std::map<id_type, SecurityParameters> map_;
   SecurityParameters params_;
};
}  // namespace mp

#endif  // MASTER_PROJECT_MASTER_SECRETS_H_
