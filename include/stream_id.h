/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_STREAM_ID_H_
#define MASTER_PROJECT_STREAM_ID_H_

#include <cstdint>
#include <tins/tins.h>

class stream_id {
 public:
  Tins::IPv4Address src_addr;
  uint16_t src_port;
  Tins::IPv4Address dst_addr;
  uint16_t dst_port;

  friend bool operator==(const stream_id &lhs, const stream_id &rhs);
  friend bool operator!=(const stream_id &lhs, const stream_id &rhs); 
  std::size_t hash() const;

 private:
  uint64_t src() const;
  uint64_t dst() const;
  uint64_t combine(const Tins::IPv4Address &address, uint64_t port) const;
};

namespace std {
  template<>
  struct hash<stream_id> {
  std::size_t operator()(const stream_id &sid) const {
    return sid.hash();
  }
  };
} // std

#endif // MASTER_PROJECT_STREAM_ID_H_

