/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_CLIENT_HELLO_H_
#define MASTER_PROJECT_CLIENT_HELLO_H_

#include <array>

#include "handshake.h" 
#include "security_parameters.h" 
#include "tls_record.h"

namespace mp {
namespace client_hello {
  // {ContentType, ProtocolVersion (major, minor)}
  using client_hello_t = std::array<char, 3>;

  static const client_hello_t sslv30{ContentType::HANDSHAKE, 3, 0};
  static const client_hello_t tlsv10{ContentType::HANDSHAKE, 3, 1};
  static const client_hello_t tlsv11{ContentType::HANDSHAKE, 3, 2};
  static const client_hello_t tlsv12{ContentType::HANDSHAKE, 3, 3};
}  // namespace client_hello

class ClientHello : public Handshake {
  public:
    explicit ClientHello(std::vector<uint8_t> &data);

    void SetRandom(SecurityParameters *params) override;
    std::array<uint8_t, 32> random();

  private: 
    TINS_BEGIN_PACK
      struct ClientHelloData {
        ProtocolVersion protocol_version;
        Random random;
        SessionId session_id;
        // cipher_suites
        // compression_methods
        // extensions
      } TINS_END_PACK;
    ClientHelloData data_;
};
}  // namespace mp
#endif  // MASTER_PROJECT_CLIENT_HELLO_H_
