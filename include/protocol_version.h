/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_PROTOCOL_VERSION_H_
#define MASTER_PROJECT_PROTOCOL_VERSION_H_

#include <tins/tins.h>
#include <cstdint>
#include <ostream>

namespace mp {
TINS_BEGIN_PACK
struct ProtocolVersion {
  uint8_t version_major;
  uint8_t version_minor;
} TINS_END_PACK;

std::ostream& operator<<(std::ostream& stream, const ProtocolVersion &version); 
}  // namespace mp
#endif // MASTER_PROJECT_PROTOCOL_VERSION_H_

