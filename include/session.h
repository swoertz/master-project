/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_SESSION_H_
#define MASTER_PROJECT_SESSION_H_

// FIXME preprocessor macro name clashes with name in tins/dns.h
// already reported and going to be fixed in a future release of libtins
// remove include when workaround is no longer necessary
#include <tins/tins.h>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/utility.hpp>

#include <array>
#include <map>
#include <memory>
#include <string>

#include "certificate_generator.h"
#include "client_hello.h"
#include "master_secrets.h"

namespace mp {
class Session
    : public std::enable_shared_from_this<Session>
    , boost::noncopyable {
 public:
    typedef boost::asio::ssl::stream<boost::asio::ip::tcp::socket &>
        ssl_socket_t;
    Session(boost::asio::ip::tcp::socket client_socket
        , boost::asio::ip::tcp::socket server_socket
        , const bool insecure
        , const std::shared_ptr<CertificateGenerator> &certificate_generator
        , const std::shared_ptr<MasterSecrets> &master_secrets
        , const std::shared_ptr<std::map<uint16_t, bool>> &ssl_ports);

    ~Session();

    void Start();

 private:
    struct SessionEndpoint {
        explicit SessionEndpoint(boost::asio::ip::tcp::socket s)
            : context(boost::asio::ssl::context::tlsv12)
            , socket(std::move(s))
            , ssl(socket, context) {
        }
        std::array<char, 1024> buffer;
        boost::asio::ssl::context context;
        boost::asio::ip::tcp::socket socket;
        ssl_socket_t ssl;
    };

    void ClientHandleHandshake(boost::system::error_code error);

    void ConfigureClientServernameCallback();

    void ConfigureCertificate(SSL *ssl);

    void ConfigureKey(SSL *ssl);

    void ConfigureServerHandshakeVerification();

    bool IsSSL();

    void Read(SessionEndpoint *endpoint, SessionEndpoint *otherEndpoint);

    void ReadInitalBytes(boost::system::error_code error);

    void ServerHandleHandshake(const boost::system::error_code &error);

    void ShutDown();

    void StartBridging();

    template<class T>
    void ReadFromStream(T *stream, SessionEndpoint *endpoint
        , SessionEndpoint *otherEndpoint);

    template<class T>
    void WriteToStream(T *stream, SessionEndpoint *endpoint
        , SessionEndpoint *otherEndpoint, size_t length);

    void Write(SessionEndpoint *endpoint, SessionEndpoint *otherEndpoint
        , std::size_t length);

    bool VerifyCallback(bool preverified
        , boost::asio::ssl::verify_context &ctx);

    static int ServerNameCallback(SSL *ssl, int *ad, void *arg);

    client_hello::client_hello_t client_hello_;

    SessionEndpoint downstream_;
    SessionEndpoint upstream_;

    bool insecure_;
    bool ssl_;

    boost::asio::ip::tcp::endpoint original_destination_;
    boost::asio::ip::tcp::endpoint original_source_;

    std::string servername_;

    std::shared_ptr<CertificateGenerator> certificate_generator_;
    std::shared_ptr<MasterSecrets> master_secrets_;
    std::shared_ptr<std::map<uint16_t, bool>> ssl_ports_;
};
}  // namespace mp

#endif  // MASTER_PROJECT_SESSION_H_
