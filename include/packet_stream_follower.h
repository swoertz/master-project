/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <map>
#include <memory>
#include <vector>

#include <tins/tins.h>
#include <tins/tcp_ip/stream.h>
#include <tins/tcp_ip/stream_follower.h>
#include <tins/tcp_ip/stream_identifier.h>

#include "master_secrets.h" 
#include "tls_record.h" 

namespace mp {

class PacketStreamFollower {
  public:
    PacketStreamFollower(Tins::BaseSniffer *sniffer
        , MasterSecrets *master_secrets
        , const std::string &filename);
    bool ProcessPacket(Tins::Packet &packet);
  private:
    void InitCallbacks(Tins::TCPIP::Stream &stream);
    void OnClientData(Tins::TCPIP::Stream &stream);
    void OnNewStream(Tins::TCPIP::Stream &stream);
    void OnServerData(Tins::TCPIP::Stream &stream);
    void OnStreamClosed(Tins::TCPIP::Stream &stream);
    void OnOutOfOrder(Tins::TCPIP::Stream &stream);

    bool last_packet_data_callback_called_;
    bool last_packet_out_of_order_callback_called_;
    
    using packets_type = std::map<Tins::TCPIP::StreamIdentifier
      , std::vector<std::unique_ptr<Tins::PDU>>>;
    packets_type client_packets_;
    packets_type server_packets_;
    std::vector<std::unique_ptr<TlsRecord>> tls_records_;
    MasterSecrets* master_secrets_;
    Tins::TCPIP::StreamFollower follower_;
    Tins::PacketWriter packet_writer_;
};
}
