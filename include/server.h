/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_SERVER_H_
#define MASTER_PROJECT_SERVER_H_

#include <boost/asio.hpp>

#include <map>
#include <memory>
#include <string>
#include <vector>

#include "certificate_generator.h"
#include "master_secrets.h"

namespace mp {
class Server {
 public:
    Server(const std::shared_ptr<boost::asio::io_service> &io_service
        , const unsigned short port
        , const bool insecure
        , const bool transparent
        , const std::shared_ptr<CertificateGenerator> &certificate_generator
        , const std::shared_ptr<MasterSecrets> &master_secrets
        , const std::vector<uint16_t> &ssl_ports
        , const std::vector<uint16_t> &non_ssl_ports);

    void Shutdown();

 private:
    bool InitAcceptor(unsigned short port, bool transparent);
    void Loop();
    void SetupTransparentMode();

    boost::asio::ip::tcp::acceptor acceptor_;
    boost::asio::ip::tcp::socket client_;
    boost::asio::ip::tcp::socket server_;
    bool insecure_;
    std::shared_ptr<boost::asio::io_service> io_service_;
    std::shared_ptr<CertificateGenerator> certificate_generator_;
    std::shared_ptr<MasterSecrets> master_secrets_;
    std::shared_ptr<std::map<uint16_t, bool>> ssl_ports_;
};
}  // namespace mp

#endif  // MASTER_PROJECT_SERVER_H_
