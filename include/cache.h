/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_CACHE_H_
#define MASTER_PROJECT_CACHE_H_

#include <list>
#include <string>
#include <unordered_map>
#include <utility>

#include "x509.h"

namespace mp {
class Cache {
 public:
  using key_t = std::string;
  using value_t = X509;
  using key_value_t = std::pair<key_t, value_t>;
  using key_value_iterator_t = std::list<key_value_t>::iterator;

  void Put(key_t key, value_t value);
  value_t Get(const key_t &key);
 private:
  std::list<key_value_t> list_;
  std::unordered_map<key_t, key_value_iterator_t> map_;

};
}  // namespace mp

#endif  // MASTER_PROJECT_CACHE_H_
