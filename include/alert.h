/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_ALERT_H_
#define MASTER_PROJECT_ALERT_H_

#include <tins/tins.h>

namespace mp {
enum AlertLevel{
  WARNING = 1,
  FATAL = 2
};

enum AlertDescription{
  CLOSE_NOTIFY = 0,
  UNEXPECTED_MESSAGE = 10,
  BAD_RECORD_MAC = 20,
  DECRYPTION_FAILED_reserved = 21,
  RECORD_OVERFLOW = 22,
  DECOMPRESSION_FAILURE = 30,
  ALERT_FAILURE = 40,
  NO_CERTIFICATE_reserved = 41,
  BAD_CERTIFICATE = 42,
  UNSUPPORTED_CERTIFICATE = 43,
  CERTIFICATE_REVOKED = 44,
  CERTIFICATE_EXPIRED = 45,
  CERTIFICATE_UNKNOWN = 46,
  ILLEGAL_PARAMETER = 47,
  UNKNOWN_CA = 48,
  ACCESS_DENIED = 49,
  DECODE_ERROR = 50,
  DECRYPT_ERROR = 51,
  EXPORT_RESTRICTION_reserved = 60,
  PROTOCOL_VERSION = 70,
  INSUFFICIENT_SECURITY = 71,
  INTERNAL_ERROR = 80,
  USER_CANCELED = 90,
  NO_RENEGOTIATION = 100,
  UNSUPPORTED_EXTENSION = 110
};

TINS_BEGIN_PACK
struct AlertData{
  uint8_t level;
  uint8_t description;
} TINS_END_PACK;

class Alert {
  public:
    Alert() {};
    Alert(const uint8_t* buffer, uint32_t size);
    AlertDescription alert_description() { return static_cast<AlertDescription>(data_.description); };
    AlertLevel alert_level() { return static_cast<AlertLevel>(data_.level); };

    friend std::ostream& operator<<(std::ostream &stream, const Alert &alert);

  private:
    AlertData data_;
};
}  // namespace mp
#endif // MASTER_PROJECT_ALERT_H_

