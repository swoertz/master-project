/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_SERVER_HELLO_H_
#define MASTER_PROJECT_SERVER_HELLO_H_

#include "handshake.h"
#include "security_parameters.h"

namespace mp {
class ServerHello : public Handshake {
  public:
    explicit ServerHello(std::vector<uint8_t> &data);

    std::array<uint8_t, sizeof(Random)> random(); 
    std::array<uint8_t, sizeof(SessionId)> session_id(); 

    void SetRandom(SecurityParameters* params) override;

  private: 
    TINS_BEGIN_PACK
    struct ServerHelloData {
      ProtocolVersion protocol_version;
      Random random;
      SessionId session_id;
      CipherSuite cipher_suite;
      CompressionMethod compression_method;
      // extensions
    } TINS_END_PACK;

    ServerHelloData data_;
};
}  // namespace mp
#endif  // MASTER_PROJECT_SERVER_HELLO_H_

