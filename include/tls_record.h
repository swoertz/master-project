/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_TLS_RECORD_H_
#define MASTER_PROJECT_TLS_RECORD_H_

#include <cstdint>
#include <iterator>
#include <memory>
#include <string>
#include <tins/endianness.h>
#include <tins/tins.h>

#include "protocol_version.h"
#include "security_parameters.h"

namespace mp {
TINS_BEGIN_PACK
struct TlsRecordHeader {
  uint8_t content_type;
  ProtocolVersion protocol_version;
  uint16_t length;
} TINS_END_PACK;

enum ContentType{
  CHANGE_CIPHER_SPEC = 20,
  ALERT = 21,
  HANDSHAKE = 22,
  APPLICATION_DATA = 23
};

class TlsRecord {
  public: 
  explicit TlsRecord(std::vector<uint8_t> &data);
  virtual ~TlsRecord() {};
  ContentType content_type() const { return static_cast<ContentType>(header_.content_type); };
  uint16_t length() const { return Tins::Endian::be_to_host(header_.length); };
  ProtocolVersion protocol_version() const { return header_.protocol_version; };
  virtual std::string Serialize() const;
  virtual void SetRandom(SecurityParameters *) {}
  virtual void Decrypt() {}
  friend std::ostream& operator<<(std::ostream& stream, const TlsRecord &record); 
  private:
   TlsRecordHeader header_;
  protected:
   std::vector<uint8_t>::iterator body_;
};
}  // namespace mp
#endif  // MASTER_PROJECT_TLS_RECORD_H_

