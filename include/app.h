/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_APP_H_
#define MASTER_PROJECT_APP_H_

#include <boost/asio.hpp>
#include <boost/log/trivial.hpp>
#include <boost/program_options.hpp>

#include <memory>
#include <string>
#include <vector>

#include "server.h"
#include "master_secrets.h"

namespace mp {
class App {
 public:
    App();
    int Run(int argc, char **argv);

 private:
    void ExitHandlerGraceful();
    void ExitHandlerForceful();
    void DecryptionThread();
    void InitDescription();
    void InitLogging();
    void InitServer();
    void InitSigINT();
    void LogConfig();
    void MainThread();
    void MitMThread();
    void ParseConfig(int argc, char **argv);
    void PrintUsage();
    void StoreProgramName(char **argv);

    std::shared_ptr<mp::MasterSecrets> master_secrets_;
    std::shared_ptr<boost::asio::io_service> network_ios_;
    std::shared_ptr<mp::Server> server_;

    std::string ca_certfile_;
    std::string ca_keyfile_;
    std::string ca_serialfile_;
    std::string configfile_;
    std::string interface_;
    std::string outputfile_;
    std::string program_name_;

    int port_;

    bool help_;
    bool insecure_;
    bool transparent_;

    std::vector<uint16_t> ssl_ports_;
    std::vector<uint16_t> non_ssl_ports_;

    boost::asio::io_service signal_ios_;
    boost::asio::signal_set signals_;

    boost::log::trivial::severity_level log_level_;

    boost::program_options::options_description description_;
    boost::program_options::variables_map config_;
};
}  // namespace mp

#endif  // MASTER_PROJECT_APP_H_
