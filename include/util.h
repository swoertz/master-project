/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MASTER_PROJECT_UTIL_H_
#define MASTER_PROJECT_UTIL_H_

#include <cstddef>

#include <botan/botan.h>

#include "security_parameters.h"

namespace mp {
std::size_t minimum_block_length(const SecurityParameters &parameters);

struct DecryptionContext {
  Botan::secure_vector<Botan::byte> client_write_key;
  Botan::secure_vector<Botan::byte> client_write_iv;
  Botan::secure_vector<Botan::byte> server_write_key;
  Botan::secure_vector<Botan::byte> server_write_iv;
};

DecryptionContext partition_key_block(
    const Botan::secure_vector<Botan::byte> &key_block
    , const SecurityParameters &parameters);

Botan::secure_vector<Botan::byte> expand_key(
    const SecurityParameters &parameters);
}  // namespace mp

#endif // MASTER_PROJECT_UTIL_H_

