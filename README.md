# Installation:
see `install.sh` for Arch Linux

# Requirements:
library | version
------- | -------
libtins | 3.5
openssl | 1.1.0
botan   | 2.0
boost   | 1.54

see `install.sh` for more information 

# License
see `LICENSE` file

(c) Simon Woertz 2017
