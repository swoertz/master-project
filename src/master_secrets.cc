/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "master_secrets.h"

#include <boost/log/trivial.hpp>
#include <sstream> 

namespace mp {
MasterSecrets::MasterSecrets() : params_{AES_256_SHA} {
}

void MasterSecrets::Add(const std::string &ip_address, const uint16_t port 
   , const Botan::secure_vector<Botan::byte> &master_key) {
   SecurityParameters &params = params_; 
   auto id = Id(ip_address, port);
   params.client_ip = ip_address;
   params.client_port = port;
   std::copy(master_key.begin(),master_key.end()
       , params.master_secret.begin());
   map_.emplace(id, params);
}

MasterSecrets::id_type MasterSecrets::Id(const std::string &ip_address
    , const uint16_t port) {
  std::stringstream id;
  id << ip_address << ":" << port;
  return id.str();
}

void MasterSecrets::ClientRandom(const std::string &ip_address
    , const uint16_t port 
    , const std::array<uint8_t, 32> &client_random) {
  auto id = Id(ip_address, port);
  auto &params = map_[id];
  std::copy(client_random.begin(),client_random.end()
       , params.client_random.begin());
}

void MasterSecrets::ServerRandom(const std::string &ip_address
    , const uint16_t port 
    , const std::array<uint8_t, 32> &server_random) {
  auto id = Id(ip_address, port);
  auto &params = map_[id];
  std::copy(server_random.begin(),server_random.end()
       , params.server_random.begin());
}

SecurityParameters* MasterSecrets::Get(const std::string &src_ip
    , const uint16_t src_port
    , const std::string &dst_ip
    , const uint16_t dst_port) {
  //TODO return std::shared_ptr instead? 
  auto sid = Id(src_ip, src_port);
  auto did = Id(dst_ip, dst_port);
  auto it = map_.find(sid);
  if(it != map_.end()) {
    BOOST_LOG_TRIVIAL(info) << "found SecurityParameters for " << sid;
    return &it->second;
  }
  it = map_.find(did);
  if(it != map_.end()) {
    BOOST_LOG_TRIVIAL(info) << "found SecurityParameters for " << did;
    return &it->second;
  }
  BOOST_LOG_TRIVIAL(warning) << "SecurityParameters not found for " << sid 
    <<  "->" << did << "!";
  return nullptr;
} 
}  // namespace mp

