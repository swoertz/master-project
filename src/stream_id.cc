/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stream_id.h"

#include <boost/functional/hash.hpp>

uint64_t stream_id::combine(const Tins::IPv4Address &address, const uint64_t port) const {
  return (port << 32) ^ address;
}

std::size_t stream_id::hash() const {
  std::size_t seed = 0;
  auto s = src();
  auto d = dst();
  if(s < d) {
    boost::hash_combine(seed, boost::hash_value(s));
    boost::hash_combine(seed, boost::hash_value(d));
  } else {
    boost::hash_combine(seed, boost::hash_value(d));
    boost::hash_combine(seed, boost::hash_value(s));
  }
  return seed;
}

uint64_t stream_id::src() const {
  return combine(src_addr, src_port);
}

uint64_t stream_id::dst() const {
  return combine(dst_addr, dst_port); 
}

bool operator==(const stream_id &lhs, const stream_id &rhs) {
  if((lhs.src() == rhs.src() && lhs.dst() == rhs.dst())
      || (lhs.src() == rhs.dst() && lhs.dst() == rhs.src())) {
    return true;
  }
  return false;
}

bool operator!=(const stream_id &lhs, const stream_id &rhs) {
  return !(lhs == rhs);
}

