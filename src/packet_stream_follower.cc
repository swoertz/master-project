/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "packet_stream_follower.h"

#include <cstdlib>
#include <functional>
#include <utility>

#include <boost/log/trivial.hpp>
#include <botan/botan.h>
#include <botan/cipher_mode.h>
#include <botan/hex.h>
#include <botan/lookup.h>
#include <botan/key_filt.h>
#include <botan/pipe.h>

#include "stream_util.h"
#include "tls_record_factory.h"
#include "util.h"

namespace {
enum class Endpoint {
  CLIENT, SERVER
};

Botan::secure_vector<Botan::byte> GetKey(mp::SecurityParameters *ms
    , Endpoint end) {
    auto key_block = expand_key(*ms);
    auto ctx = partition_key_block(key_block, *ms);
    return end == Endpoint::CLIENT ? ctx.client_write_key
      : ctx.server_write_key;
}

bool pop_back_n(std::string & str, std::size_t n) {
  if(n > str.size()) {
    BOOST_LOG_TRIVIAL(warning) << "cannot truncate this string that much";
    return false;
  }
  str.erase(str.size() - n, n);
  return true;
}

void RemovePadding(std::string &str) {
  // ... | PKCS7 | padding length
  //TODO check padding
  uint8_t padding_length = str.back();
  pop_back_n(str, padding_length + 1);
}

void RemoveMAC(std::string &str, mp::SecurityParameters *ms) {
  pop_back_n(str, ms->mac_length);
}

std::string Decrypt(mp::SecurityParameters *ms, mp::TlsRecord *tls
    , Endpoint endpoint) {
  //TODO remove unnecessary strings
  std::string plaintext;
  std::string plaintext_without_padding;
  std::string plaintext_without_mac;
  if(auto ad= dynamic_cast<mp::ApplicationData*>(tls)) {
    auto ap_payload = ad->data();
    // TODO compute key once
    auto key = GetKey(ms, endpoint);
    auto iv = Botan::secure_vector<Botan::byte>(ap_payload.begin()
        , ap_payload.begin() + ms->record_iv_length);
    Botan::Pipe pipe(Botan::get_cipher("AES-256/CBC/NoPadding", key
          , iv, Botan::DECRYPTION));
    auto ciphertext = Botan::secure_vector<Botan::byte>(ap_payload.begin()
        + ms->record_iv_length, ap_payload.end());
    pipe.process_msg(ciphertext);
    auto bytes = pipe.read_all(0);
    plaintext_without_padding = std::string(bytes.begin(), bytes.end());
    RemovePadding(plaintext_without_padding);
    plaintext_without_mac = plaintext_without_padding;
    RemoveMAC(plaintext_without_mac, ms);
  }
  return plaintext_without_mac;
}
}  // anonymous namespace

namespace mp {
PacketStreamFollower::PacketStreamFollower(Tins::BaseSniffer *sniffer
  , MasterSecrets *master_secrets, const std::string &filename)
  : master_secrets_{master_secrets}
  , packet_writer_{filename, Tins::DataLinkType<Tins::EthernetII>()} {
  follower_.new_stream_callback(std::bind(&PacketStreamFollower::OnNewStream,
        this, std::placeholders::_1));
  sniffer->sniff_loop(std::bind(&PacketStreamFollower::ProcessPacket, this,
        std::placeholders::_1));
}

void PacketStreamFollower::InitCallbacks(Tins::TCPIP::Stream &stream) {
  stream.client_data_callback(std::bind(&PacketStreamFollower::OnClientData
        , this, std::placeholders::_1));
  stream.server_data_callback(std::bind(&PacketStreamFollower::OnServerData
        , this, std::placeholders::_1));
  stream.stream_closed_callback(std::bind(
        &PacketStreamFollower::OnStreamClosed, this, std::placeholders::_1));
  stream.client_out_of_order_callback(std::bind(
        &PacketStreamFollower::OnOutOfOrder, this, std::placeholders::_1));
  stream.server_out_of_order_callback(std::bind(
        &PacketStreamFollower::OnOutOfOrder, this, std::placeholders::_1));
}

bool PacketStreamFollower::ProcessPacket(Tins::Packet &packet) {
  // reset flags to be able to determine if callbacks were called during
  // the processing of this packet
  last_packet_data_callback_called_ = false;
  last_packet_out_of_order_callback_called_ = false;
  // clone to have a copy to temporary store PDU
  std::unique_ptr<Tins::PDU> pdu(packet.pdu()->clone());
  Tins::IP& ip = pdu->rfind_pdu<Tins::IP>();
  Tins::TCP& tcp = pdu->rfind_pdu<Tins::TCP>();
  std::unique_ptr<Tins::TCPIP::Stream> stream;
  follower_.process_packet(packet);
  Endpoint endpoint;
  try {
    // extract quintuple from pdu
    auto id = Tins::TCPIP::StreamIdentifier::make_identifier(*pdu);
    stream = std::make_unique<Tins::TCPIP::Stream>(follower_.find_stream(
          ip.src_addr(), tcp.sport(), ip.dst_addr(), tcp.dport()));
    if(stream->client_flow().packet_belongs(*pdu)) {
      endpoint = Endpoint::CLIENT;
      auto packets = client_packets_.find(id);
      if(packets != client_packets_.end()) {
        packets->second.emplace_back(std::move(pdu));
      }
    } else {
      endpoint = Endpoint::SERVER;
      auto packets = server_packets_.find(id);
      if(packets != server_packets_.end()) {
        packets->second.emplace_back(std::move(pdu));
      }
    }
  } catch(const Tins::stream_not_found &e) {
    BOOST_LOG_TRIVIAL(warning) << "stream not found";
  }
  if(last_packet_data_callback_called_) {
    BOOST_LOG_TRIVIAL(info) << "data callback called";
    BOOST_LOG_TRIVIAL(info) << "tls records size: " << tls_records_.size();
    int app_data_counter = 0;
    for(auto &record : tls_records_) {
      if(dynamic_cast<ApplicationData*>(record.get())) {
        app_data_counter++;
        SecurityParameters *ms = master_secrets_->Get(ip.src_addr().to_string()
          , tcp.sport(), ip.dst_addr().to_string(), tcp.dport());
        auto plaintext = Decrypt(ms, record.get(), endpoint); 
        packet.pdu()->rfind_pdu<Tins::TCP>().inner_pdu(
          Tins::RawPDU(plaintext.begin(), plaintext.end()));
        packet_writer_.write(packet);
      } 
    }
    if(app_data_counter == 0) {
      // export other TLS messages (Handshake, CCS,...)
      packet_writer_.write(packet);
    }
    BOOST_LOG_TRIVIAL(info) << "app data records: " << app_data_counter;
    tls_records_.clear();
  } else {
    packet_writer_.write(packet);
  }
  return true;
}

void PacketStreamFollower::OnClientData(Tins::TCPIP::Stream &stream) {
  last_packet_data_callback_called_ = true;
  auto id = MakeStreamIdentifier(stream);
  auto it = client_packets_.find(id);
  Tins::IP *ip;
  Tins::TCP *tcp;
  SecurityParameters* ms = nullptr;
  if(it != client_packets_.end()) {
    auto pdu = it->second.front().get();
    ip = pdu->find_pdu<Tins::IP>();
    tcp = pdu->find_pdu<Tins::TCP>();
    ms = master_secrets_->Get(ip->src_addr().to_string(), tcp->sport()
        , ip->dst_addr().to_string(), tcp->dport());
  }
  if(!ms) {
    return;
  }
  auto &payload = stream.client_payload();
  while(payload.size()) {
    TlsRecordFactory factory;
    auto tls = factory.create(payload);
    if(tls) {
      if(ms) {
        tls->SetRandom(ms);
          std::string plaintext = Decrypt(ms, tls.get(), Endpoint::CLIENT);
        payload.erase(payload.begin(), payload.begin() + tls->length()
            + sizeof(TlsRecordHeader));
        tls_records_.emplace_back(std::move(tls));
      }
    } else {
      break;
    }
  }
}

void PacketStreamFollower::OnOutOfOrder(Tins::TCPIP::Stream &/*stream*/) {
  last_packet_out_of_order_callback_called_ = true;
}

void PacketStreamFollower::OnNewStream(Tins::TCPIP::Stream &stream) {
  BOOST_LOG_TRIVIAL(info) << "new stream";
  InitCallbacks(stream);
  //init vectors to store PDUs
  auto id = MakeStreamIdentifier(stream);
  client_packets_.emplace(id, std::vector<std::unique_ptr<Tins::PDU>>());
  server_packets_.emplace(id, std::vector<std::unique_ptr<Tins::PDU>>());
  //disable payload cleanup after callbacks are called
  //cleanup is done manually
  stream.auto_cleanup_payloads(false);
}

void PacketStreamFollower::OnServerData(Tins::TCPIP::Stream &stream) {
  last_packet_data_callback_called_ = true;
  auto id = MakeStreamIdentifier(stream);
  auto it = server_packets_.find(id);
  SecurityParameters* ms = nullptr;
  Tins::IP *ip;
  Tins::TCP *tcp;
  if(it != server_packets_.end()) {
    auto pdu = it->second.front().get();
    ip = pdu->find_pdu<Tins::IP>();
    tcp = pdu->find_pdu<Tins::TCP>();
    ms = master_secrets_->Get(ip->src_addr().to_string(), tcp->sport()
        , ip->dst_addr().to_string(), tcp->dport());
  }
  if(!ms) {
    return;
  }
  auto &payload = stream.server_payload();
  while(payload.size()) {
    TlsRecordFactory factory;
    auto tls = factory.create(payload);
    if(tls) {
      if(ms) {
        tls->SetRandom(ms);
        if(dynamic_cast<ServerHello*>(tls.get())) {
          BOOST_LOG_TRIVIAL(debug) << "server random: " << Botan::hex_encode(ms->server_random.data(), ms->server_random.size(), true);
        }
        std::string plaintext = Decrypt(ms, tls.get(), Endpoint::SERVER);
        payload.erase(payload.begin(), payload.begin() + tls->length()
            + sizeof(TlsRecordHeader));
        tls_records_.emplace_back(std::move(tls));
      }
    } else {
      // not possible to construct a TLS record from the payload
      break;
    }
  }
}

void PacketStreamFollower::OnStreamClosed(Tins::TCPIP::Stream &/*stream*/) {
  BOOST_LOG_TRIVIAL(info) << "stream closed";
  //TODO delete stream's PDUs
}
}  // namespace mp

