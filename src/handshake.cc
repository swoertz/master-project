/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "handshake.h"

#include <algorithm>
#include <iterator>
#include <sstream>

namespace mp{
Handshake::Handshake(std::vector<uint8_t> &data) : TlsRecord(data) {
  // move the iterator to the first byte containing the handshake body
  body_ = std::next(TlsRecord::body_, sizeof(HandshakeHeader));
  // copy the the data between the TLS-Record header and the handshake body
  // a.k.a. the handshake header to the corresponding struct 
  std::copy(TlsRecord::body_, body_, reinterpret_cast<uint8_t*>(&header_));
}

uint32_t Handshake::length() const {
#if TINS_IS_LITTLE_ENDIAN
  return header_.length[0] + (header_.length[1] << 8) 
    + (header_.length[2] << 16);
#elif TINS_IS_BIG_ENDIAN
  return header_.length[2] + (header_.length[1] << 8)
    + (header_.length[0] << 16);
#endif
}

std::string Handshake::Serialize() const {
  std::stringstream stream;
  switch(msg_type()) {
  case HELLO_REQUEST:
    stream << "Hello Request";
    break;
  case CLIENT_HELLO:
    stream << "Client Hello";
    break;
  case SERVER_HELLO:
    stream << "Server Hello";
    break;
  case CERTIFICATE:
    stream << "Certificate";
    break;
  case SERVER_KEY_EXCHANGE:
    stream << "Server Key Exchange";
    break;
  case CERTIFICATE_REQUEST:
    stream << "Certificate Request";
    break;
  case SERVER_HELLO_DONE:
    stream << "Server Hello Done";
    break;
  case CERTIFICATE_VERIFY:
    stream << "Certificate Verify";
    break;
  case CLIENT_KEY_EXCHANGE:
    stream << "Client Key Exchange";
    break;
  case FINISHED:
    stream << "Finished";
    break;
  default:
     stream << static_cast<int>(msg_type());
     break;
  }  
  return TlsRecord::Serialize() + " Handshake " + stream.str();
}

std::ostream& operator<<(std::ostream &stream, const Handshake &handshake) {
  return stream << handshake.Serialize();
}
}  // namespace mp

