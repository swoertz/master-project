/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "x509.h"

#include <boost/log/trivial.hpp>
#include <openssl/bn.h>
#include <openssl/err.h>
#include <openssl/pem.h>

#include <algorithm>
#include <cerrno>
#include <cstring>
#include <iostream>
#include <new>
#include <stdexcept>
#include <string>

namespace mp {
X509::X509() {
    ptr_ = ::X509_new();
    CheckInternalPointerAndRaiseException();
}

X509::X509(const X509 &that) {
    ptr_ = ::X509_dup(that.ptr_);
    CheckInternalPointerAndRaiseException();
}

X509::X509(X509 &&that) noexcept {
    ptr_ = that.ptr_;
    that.ptr_ = nullptr;
}

X509::X509(const std::string &certificateFile) {
    FILE *fp = std::fopen(certificateFile.c_str(), "r");
    if (fp == nullptr) {
        // std::strerror is not thread-safe
        std::string error{std::strerror(errno)};
        BOOST_LOG_TRIVIAL(error) << error <<  ": "
            << certificateFile;
        throw std::runtime_error(error);
    }
    ptr_ = ::PEM_read_X509(fp, nullptr, nullptr, nullptr);
    std::fclose(fp);
    if (ptr_ == nullptr) {
        std::string error{ERR_error_string(ERR_get_error(), nullptr)};
        BOOST_LOG_TRIVIAL(error) << error;
        throw std::runtime_error(error);
    }
}

X509::~X509() noexcept {
    ::X509V3_EXT_cleanup();
    ::X509_free(ptr_);
}

X509& X509::operator=(const X509 &that) {
    ptr_ = ::X509_dup(that.ptr_);
    CheckInternalPointerAndRaiseException();
    return *this;
}

X509& X509::operator=(X509 &&that) noexcept {
    std::swap(ptr_, that.ptr_);
    return *this;
}

void X509::CheckInternalPointerAndRaiseException() {
    if(!ptr_) {
        auto error = ERR_error_string(ERR_get_error(), nullptr);
        BOOST_LOG_TRIVIAL(error) << error;
        throw std::bad_alloc();
    }
}

int X509::PemWrite(FILE *fp) const {
    return ::PEM_write_X509(fp, ptr_);
}

int X509::PrintFp(FILE *fp) const {
    return ::X509_print_fp(fp, ptr_);
}

int X509::SetVersion(int64_t version) {
    return ::X509_set_version(ptr_, version);
}

int X509::SetPubKey(EVP_PKEY pkey) {
    return ::X509_set_pubkey(ptr_, pkey.NativeHandle());
}

int X509::SetPubKey(::EVP_PKEY *pkey) {
    return ::X509_set_pubkey(ptr_, pkey);
}

int X509::SetSerialNumber(int64_t serial) {
    return ::ASN1_INTEGER_set(::X509_get_serialNumber(ptr_), serial);
}

int X509::SetSerialNumber(BigNum *serial) {
    return ::X509_set_serialNumber(ptr_, BN_to_ASN1_INTEGER(
        serial->NativeHandle(), ::X509_get_serialNumber(ptr_)));
}

ASN1_INTEGER* X509::GetSerialNumber() const {
    return ::X509_get_serialNumber(ptr_);
}

ASN1_TIME* X509::GmtimeAdjustNotBefore(int64_t adj) {
    return ::X509_gmtime_adj(X509_get_notBefore(ptr_), adj);
}

ASN1_TIME* X509::GmtimeAdjustNotAfter(int64_t adj) {
    return ::X509_gmtime_adj(X509_get_notAfter(ptr_), adj);
}

int X509::X509::X509::X509::IssuerAddEntryByTxt(const char *field
    , const unsigned char *bytes) {
        return ::X509_NAME_add_entry_by_txt(GetIssuerName(), field
            , MBSTRING_ASC, bytes, -1, -1, 0);
}

int X509::X509::X509::SubjectAddEntryByTxt(const char *field
    , const unsigned char *bytes) {
        return ::X509_NAME_add_entry_by_txt(GetSubjectName(), field
            , MBSTRING_ASC, bytes, -1, -1, 0);
}

int X509::X509::SetIssuerName(X509_NAME *name) {
    return ::X509_set_issuer_name(ptr_, name);
}

int X509::Sign(EVP_PKEY pkey, const EVP_MD *md) {
    return ::X509_sign(ptr_, pkey.NativeHandle(), md);
}

::X509* X509::NativeHandle() {
    return ptr_;
}

::X509_NAME* X509::GetSubjectName() const {
    return ::X509_get_subject_name(ptr_);
}

::X509_NAME* X509::GetIssuerName() const {
    return ::X509_get_issuer_name(ptr_);
}
}  // namespace mp
