/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <util.h>

#include <botan/hex.h>

namespace mp {
std::size_t minimum_block_length(const SecurityParameters &parameters) {
  return 2 * (parameters.enc_key_length + parameters.mac_key_length
      + parameters.fixed_iv_length);
}

DecryptionContext partition_key_block(
    const Botan::secure_vector<Botan::byte> &key_block
    , const SecurityParameters &parameters) {
  DecryptionContext ctx;
  auto start = 2 * parameters.mac_key_length;
  auto end = start + parameters.enc_key_length;
  ctx.client_write_key = Botan::secure_vector<Botan::byte>(key_block.begin()
      + start, key_block.begin() + end);
  start = start + parameters.enc_key_length;
  end = start + parameters.enc_key_length;
  ctx.server_write_key = Botan::secure_vector<Botan::byte>(key_block.begin()
      + start, key_block.begin() + end);
  start = start + parameters.enc_key_length;
  end = start + parameters.fixed_iv_length;
  ctx.client_write_iv = Botan::secure_vector<Botan::byte>(key_block.begin()
      + start, key_block.begin() + end);
  start = start + parameters.fixed_iv_length;
  end = start + parameters.fixed_iv_length;
  ctx.server_write_iv = Botan::secure_vector<Botan::byte>(key_block.begin()
      + start, key_block.begin() + end);
  return ctx;
}

Botan::secure_vector<Botan::byte> expand_key(
    const SecurityParameters &parameters) {
  auto min_key_block_length = minimum_block_length(parameters);
  std::string server_random(parameters.server_random.begin()
      , parameters.server_random.end());
  std::string client_random(parameters.client_random.begin()
      , parameters.client_random.end());
  Botan::secure_vector<Botan::byte> master_secret(
      parameters.master_secret.begin(), parameters.master_secret.end());
  std::string prf_id{"HMAC(SHA-256)"};
  Botan::secure_vector<Botan::byte> seed;
  Botan::secure_vector<Botan::byte> label{'k','e','y',' '
    ,'e','x','p','a','n','s','i','o','n'};
  seed.insert(seed.begin(), label.begin(), label.end());
  seed.insert(std::next(seed.begin(),label.size()), server_random.begin()
      , server_random.end());
  seed.insert(std::next(seed.begin(),label.size() + server_random.size())
      , client_random.begin(), client_random.end());
  auto key_block = Botan::secure_vector<Botan::byte>();
  auto prf = Botan::MessageAuthenticationCode::create(prf_id);
  prf->set_key(master_secret);
  auto an = prf->process(seed); //A0 = seed; A1 = HMAC(master_secret, seed)
  while(key_block.size() < min_key_block_length) {
    prf->update(an);
    auto block = prf->process(seed);
    key_block.insert(key_block.end(), block.begin(), block.end());
    an = prf->process(an);
  }
  return key_block;
}
}  // namespace mp

