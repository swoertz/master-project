/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "alert.h"

namespace mp {
Alert::Alert(const uint8_t *buffer, const uint32_t size) {
  if(size < sizeof(AlertData)) {
    throw Tins::malformed_packet();
  }
  std::memcpy(&data_, buffer, sizeof(AlertData));
  //FIXME alert data could be encrypted 
  //-> level and description has to be decrypt before interpretation
}

std::ostream& operator<<(std::ostream &stream, const Alert &alert) {
  return stream << "(" << static_cast<int>(alert.data_.level) << ", " 
    << static_cast<int>(alert.data_.description) << ")";
}
}  // namespace mp

