/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "certificate_generator.h"

#include <boost/algorithm/string.hpp>
#include <boost/log/trivial.hpp>
#include <openssl/engine.h>

#include <cstring>
#include <fstream>
#include <ios>
#include <stdexcept>
#include <string>
#include <vector>

namespace mp {
CertificateGenerator::CertificateGenerator(const std::string &keyFile
    , const std::string &certFile
    , const std::string &serialFile)
    : cache_{}
    , caKey_{keyFile}
    , issuer_{}
    , key_{2048}
    , map_{}
    , serial_file_{serialFile}
    , serial_{} {
        if (serial_file_.empty() || !ReadSerialFromFile()) {
            serial_.SetRandom();
            BOOST_LOG_TRIVIAL(info) << "generated serial: " << serial_.ToHex();
        }
        if (!certFile.empty()) {
            ExtractIssuerName(certFile);
        }
}

CertificateGenerator::~CertificateGenerator() {
    StoreSerialInFile();
}

void CertificateGenerator::ExtractIssuerName(const std::string &filename) {
    X509 certificate(filename);
    issuer_ = ::X509_NAME_dup(certificate.GetIssuerName());
    // TODO X509_NAME_online is deprecated
    char *line = ::X509_NAME_oneline(issuer_, nullptr, 0);
    BOOST_LOG_TRIVIAL(info) << line;
    OPENSSL_free(line);
}


X509 CertificateGenerator::GenerateCertificate(const std::string &name) {
    X509 cert;
    BOOST_LOG_TRIVIAL(info) << "generate cert and store it in map";
    cert.SetVersion(2);  //X509.v3 (zero-indexed)
    SetValidity(&cert);
    SetSerial(&cert);
    // to not exchange the subject with the wildcard for tld's and second
    // layer domains
    SetSubject(&cert, name);
    SetIssuer(&cert);
    cert.SetPubKey(key_);
    cert.Sign(caKey_, EVP_sha256());
    // FIXME remove cache or map
    cache_.Put(name, cert);
    // save certificate for reuse in map 
    map_.emplace(name, cert);
    return cert;
}

X509 CertificateGenerator::GetCertificate(const std::string &subjectCN) {
    X509 cert;
    // check if the certificate map already contains a certificate with a common
    // wildcard name 
    std::string wildcard{Wildcard(subjectCN)};
    auto it = map_.find(wildcard);
    if (it != map_.end()) {
        // FIXME remove cache or map
        cache_.Get(subjectCN);
        BOOST_LOG_TRIVIAL(info) << "found cert in map";
        // set return value to the found certificate
        cert = it->second;
    } else {
        cert = GenerateCertificate(wildcard);
    }
    return cert;
}

EVP_PKEY CertificateGenerator::GetKey(const std::string * /* subjectCN */) {
    return key_;
}

bool CertificateGenerator::ReadSerialFromFile() {
    // open an input file stream
    std::ifstream file(serial_file_);
    // check if file exists
    if (file) {
        std::string hexstring;
        // read string from file 
        file >> hexstring;
        serial_.SetHex(hexstring);
        BOOST_LOG_TRIVIAL(info) << serial_.ToDec() << " (" << serial_.ToHex()
            << ")";
        return true;
    }
    std::string error{std::strerror(errno)};
    BOOST_LOG_TRIVIAL(error) << error << ": " << serial_file_;
    return false;
}

void CertificateGenerator::SetIssuer(X509 *cert) {
    if (issuer_) {
        cert->SetIssuerName(issuer_);
    } else {
        // split issuer string into '=' separated key-value pairs
        std::vector<std::string> pairs;
        boost::split(pairs, "/C=AT/ST=Tyrol/O=CA/CN=CA", boost::is_any_of("/"));
        for (std::string pair : pairs) {
            // split key-value pair into key and value
            if(!pair.empty()) {
              std::vector<std::string> keyValue;
              boost::split(keyValue, pair, boost::is_any_of("="));
              std::string key{keyValue[0]};
              std::string value{keyValue[1]};
              // add entry to the issuer part of the certifcate
              cert->IssuerAddEntryByTxt(key.c_str()
                  , (const unsigned char *) value.c_str());
            }
        }
    }
}

void CertificateGenerator::SetSerial(X509 *cert) {
    // set the serial number in the certificate to the current serial_
    cert->SetSerialNumber(&serial_);
    // increment the CertificateGenerator's serial_
    serial_.Increment();
}

void CertificateGenerator::SetSubject(X509 *cert
    , const std::string &subjectCN) {
        // set the 'common name' in the subject part of the certificate
        cert->SubjectAddEntryByTxt("CN"
            , (const unsigned char *) subjectCN.c_str());
}

void CertificateGenerator::SetValidity(X509 *cert) {
    constexpr uint64_t days{365};
    constexpr uint64_t amountOfDaysInSeconds{days * 60 * 60 * 24};
    // set the not before value of the certificate to amount of 'days' back
    cert->GmtimeAdjustNotBefore(-amountOfDaysInSeconds);
    // set the not after value of the certificate to amount of 'days' into the future
    cert->GmtimeAdjustNotAfter(amountOfDaysInSeconds);
}

bool CertificateGenerator::StoreSerialInFile() {
    // open an output file-stream
    std::ofstream file(serial_file_);
    // check if opening was possible
    if (file) {
    // write hex-value of the serial into the file
        file << serial_.ToHex();
        BOOST_LOG_TRIVIAL(info) << "storing serial in file: " << serial_file_;
        return true;
    }
    return false;
}

std::string CertificateGenerator::Wildcard(const std::string &name) {
    std::string wildcard{name};
    // calculate depth value of the domain
    // depth = 0 for a TLD, depth = 1 for a second-level domain,...
    auto depth = std::count(name.begin(), name.end(), '.');
    // index of the first '.' in the given domain name
    auto first_dot = name.find_first_of(".");
    // return wildcard certificate for third-level domains or higher levels 
    if(depth > 1) {
      // replace everything until the first dot with * (e.g. foo.bar.at -> *.bar.at)
      return  wildcard.replace(0, first_dot, "*");
    }
    return name;
}
}  // namespace mp
