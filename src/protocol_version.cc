/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <iomanip>
#include "protocol_version.h"
#include <boost/io/ios_state.hpp>

namespace mp {
std::ostream& operator<<(std::ostream& stream, const ProtocolVersion &version) {
  std::string str;
  if(version.version_major == 0x3) {
    str = "SSLv3.0";
    if(version.version_minor > 0x0) {
      str = "TLSv1." + std::to_string(version.version_minor - 1);
    }
  } 
  
  // guard saving the status of the stream before the manipulators (RAII)
  boost::io::ios_base_all_saver guard(stream);

  stream << std::hex << std::setfill('0');
  stream << str << "(0x";
  stream << std::setw(2) << (int) version.version_major;
  stream << std::setw(2) << (int) version.version_minor;
  stream << ")";

  return stream; 
} 
}

