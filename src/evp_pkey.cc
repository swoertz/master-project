/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "big_num.h"
#include "evp_pkey.h"

#include <boost/log/trivial.hpp>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/rsa.h>

#include <algorithm>
#include <cstring>
#include <new>
#include <stdexcept>
#include <string>

namespace mp {
EVP_PKEY::EVP_PKEY(int bits) {
    ptr_ = ::EVP_PKEY_new();
    CheckInternalPointerAndRaiseException();
    RSA* rsa = ::RSA_new();
    BigNum f4;
    f4.SetWord(RSA_F4); 
    ::RSA_generate_key_ex(rsa, bits, f4.NativeHandle(), NULL);
    if (rsa == nullptr) {
        auto error = ERR_error_string(ERR_get_error(), nullptr);
        BOOST_LOG_TRIVIAL(error) << error;
        throw std::bad_alloc();
    }
    if (!::EVP_PKEY_assign_RSA(ptr_, rsa)) {
        auto error = ERR_error_string(ERR_get_error(), nullptr);
        BOOST_LOG_TRIVIAL(error) << error;
        throw std::runtime_error(error);
    }

}

EVP_PKEY::EVP_PKEY(const std::string &keyfile) {
    ptr_ = ::EVP_PKEY_new();
    FILE *fp = std::fopen(keyfile.c_str(), "r");
    if (fp == nullptr) {
        // std::strerror is not thread-safe
        std::string error{std::strerror(errno)};
        BOOST_LOG_TRIVIAL(error) << error <<  ": " << keyfile;
        throw std::runtime_error(error);
    }
    RSA *rsa = ::PEM_read_RSAPrivateKey(fp, nullptr, nullptr, nullptr);
    std::fclose(fp);
    if (rsa == nullptr) {
        auto error = ERR_error_string(ERR_get_error(), nullptr);
        BOOST_LOG_TRIVIAL(error) << error;
        throw std::runtime_error(error);
    }
    if (!::EVP_PKEY_assign_RSA(ptr_, rsa)) {
        auto error = ERR_error_string(ERR_get_error(), nullptr);
        BOOST_LOG_TRIVIAL(error) << error;
        throw std::runtime_error(error);
    }
}

EVP_PKEY::~EVP_PKEY() noexcept {
    ::EVP_PKEY_free(ptr_);
}

EVP_PKEY::EVP_PKEY(const EVP_PKEY &that) {
    ptr_ = that.ptr_;
    ::EVP_PKEY_up_ref(ptr_);
}

EVP_PKEY::EVP_PKEY(EVP_PKEY &&that) noexcept {
    ptr_ = that.ptr_;
    that.ptr_ = nullptr;
}

EVP_PKEY& EVP_PKEY::operator=(const EVP_PKEY &that) {
    ptr_ = that.ptr_;
    ::EVP_PKEY_up_ref(ptr_);
    return *this;
}

EVP_PKEY& EVP_PKEY::operator=(EVP_PKEY &&that) noexcept {
    std::swap(ptr_, that.ptr_);
    return *this;
}

void EVP_PKEY::CheckInternalPointerAndRaiseException() {
    if(!ptr_) {
        auto error = ERR_error_string(ERR_get_error(), nullptr);
        BOOST_LOG_TRIVIAL(error) << error;
        throw std::bad_alloc();
    }
}

void EVP_PKEY::PemWrite() {
    auto rsa = ::EVP_PKEY_get0_RSA(ptr_);
    ::PEM_write_RSAPrivateKey(stdout, rsa, nullptr, nullptr, -1
        , nullptr, nullptr);
}

::EVP_PKEY* EVP_PKEY::NativeHandle() {
    return ptr_;
}
}  // namespace mp
