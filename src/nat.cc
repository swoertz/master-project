/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "nat.h"

#include <netinet/in.h>

#include <boost/log/trivial.hpp>

#ifdef NF_AVAILABLE
#include <linux/netfilter_ipv4.h>
#endif

namespace mp {
boost::asio::ip::tcp::endpoint
    nat::Resolve(boost::asio::ip::tcp::socket *socket) {
#ifdef NF_AVAILABLE
    struct sockaddr_in orig_dest;
    socklen_t size = sizeof(orig_dest);
    int socket_fd = static_cast<int>(socket->native());
    getsockopt(socket_fd, SOL_IP, SO_ORIGINAL_DST, &orig_dest, &size);
    BOOST_LOG_TRIVIAL(info)
        << "resolving original destination address (netfilter)";
    // TODO ipv6
    return boost::asio::ip::tcp::endpoint(
        boost::asio::ip::address_v4(ntohl(orig_dest.sin_addr.s_addr))
        , ntohs(orig_dest.sin_port));
#endif
    BOOST_LOG_TRIVIAL(info) << "no nat resolution performed";
    boost::system::error_code error;
    boost::asio::ip::tcp::endpoint endpoint{socket->local_endpoint(error)};
    if (error) {
        BOOST_LOG_TRIVIAL(error) << error.message();
    }
    return endpoint;
    }
}  // namespace mp
