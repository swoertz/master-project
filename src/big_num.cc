/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "big_num.h"

#include <algorithm>
#include <new>
#include <string>

#include <boost/log/trivial.hpp>
#include <openssl/err.h>

namespace mp {
BigNum::BigNum() {
    ptr_ = ::BN_new();
    CheckInternalPointerAndRaiseException();
}

BigNum::BigNum(const BigNum& other) {
    ptr_ = ::BN_dup(other.ptr_);
    CheckInternalPointerAndRaiseException();
}

BigNum::BigNum(BigNum&& other) noexcept {
    ptr_ = other.ptr_;
    other.ptr_ = nullptr;
}

BigNum::~BigNum() noexcept{
    ::BN_free(ptr_);
}

BigNum& BigNum::operator=(const BigNum& other) {
    BigNum tmp(other);
    *this = std::move(tmp);
    return *this;
}

BigNum& BigNum::operator=(BigNum&& other) noexcept {
    std::swap(ptr_, other.ptr_);
    return *this;
}

void BigNum::CheckInternalPointerAndRaiseException() {
    if(!ptr_) {
        auto error = ERR_error_string(ERR_get_error(), nullptr);
        BOOST_LOG_TRIVIAL(error) << error;
        throw std::bad_alloc();
    }
}

int BigNum::Increment() {
    return ::BN_add(ptr_, ptr_, ::BN_value_one());
}

::BIGNUM* BigNum::NativeHandle() {
    return ptr_;
}

int BigNum::SetHex(const std::string &hexstring) {
    return  ::BN_hex2bn(&ptr_, hexstring.c_str());
}

int BigNum::SetRandom() {
    int bits{64};    /* amount of bits */
    int top{0};      /* most significant bit is 1 */
    int bottom{0};   /* random number is even */
    return ::BN_pseudo_rand(ptr_, bits, top, bottom);
}

int BigNum::SetWord(const unsigned long word) {
    return ::BN_set_word(ptr_, word);
}

std::string BigNum::ToDec() {
    char *decimal = BN_bn2dec(ptr_);
    std::string result{decimal};
    OPENSSL_free(decimal);
    return result;
}


std::string BigNum::ToHex() {
    char *hex = ::BN_bn2hex(ptr_);
    std::string result{hex};
    ::OPENSSL_free(hex);
    return result;
}
}  // namespace mp
