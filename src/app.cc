/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "app.h"

#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/thread.hpp>

#include <botan/botan.h>
#include <botan/hex.h>

#include <tins/tins.h>

#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <sstream>
#include <string>

#include "packet_stream_follower.h"
#include "server.h"
#include "security_parameters.h"

namespace po = boost::program_options; 
namespace mp {

App::App()
    : master_secrets_{std::make_shared<MasterSecrets>()}
    , network_ios_{std::make_shared<boost::asio::io_service>()}
    , port_{8080} 
    , help_{false}
    , transparent_{true}
    , signals_{signal_ios_} {
    }
void App::ExitHandlerForceful() {
    BOOST_LOG_TRIVIAL(info) << "received SIGINT";
    if (network_ios_) {
        BOOST_LOG_TRIVIAL(info) << "stopping the io_service";
        network_ios_->stop();
    }
}

void App::ExitHandlerGraceful() {
        BOOST_LOG_TRIVIAL(info) << "received SIGINT";
        if (server_) {
            BOOST_LOG_TRIVIAL(info) << "trying to shutdown server gracefully"
                << " (next SIGINT forces application to stop)";
            server_->Shutdown();
        }
        // associate another callback for the next occuring signal
        signals_.async_wait(std::bind(&App::ExitHandlerForceful, this));
}

void App::InitDescription() {
    description_.add_options()
    ("port,p", po::value<int>(&port_)->default_value(8080)
        , "port to use for listening")
    ("CA,c", po::value<std::string>(&ca_certfile_)
        , "CA file to read issuer information from")
    ("CAkey,k", po::value<std::string>(&ca_keyfile_)->default_value("ca.key")
        , "private key to sign generated certificates")
    ("CAserial,s", po::value<std::string>(&ca_serialfile_), "serial file")
    ("interface,i", po::value<std::string>(&interface_), "interface")
    ("insecure,I", po::bool_switch(&insecure_)
        , "disable certificate verification")
    ("out,o", po::value<std::string>(&outputfile_)->default_value("out.pcap")
        , "output file")
    ("config,C", po::value<std::string>(&configfile_), "config file")
    ("log-level,l", po::value<boost::log::trivial::severity_level>(&log_level_)
    	->default_value(boost::log::trivial::info), "log level")
    ("ssl-ports,S", po::value<std::vector<uint16_t>>(&ssl_ports_)->multitoken()
        , "ssl ports")
    ("non-ssl-ports,n"
        , po::value<std::vector<uint16_t>>(&non_ssl_ports_)->multitoken()
        , "non-ssl ports")
    ("help,h", po::bool_switch(&help_), "produce help message");
}

void App::InitLogging() {
    boost::log::core::get()->set_filter
	 (
	     boost::log::trivial::severity >= log_level_ 
	 );
}

void App::InitServer() {
    auto certifcate_generator = std::make_shared<mp::CertificateGenerator>
        (ca_keyfile_, ca_certfile_ , ca_serialfile_);
    server_ = std::make_shared<Server>(network_ios_, port_, insecure_
       , transparent_, certifcate_generator, master_secrets_, ssl_ports_
       , non_ssl_ports_);
}

void App::InitSigINT() {
    signals_.add(SIGINT);
    signals_.async_wait(std::bind(&App::ExitHandlerGraceful, this));
    BOOST_LOG_TRIVIAL(info) << "configured SIGINT callback handler";
}

// iterate over map and print key-value pairs for int, bool and std:string
void App::LogConfig() {
    BOOST_LOG_TRIVIAL(info)
        << "program running with the following configuration";
    for (const auto& it : config_) {
         const std::string &name{it.first};
         const po::variable_value &value{it.second};
         if (!value.empty()) {
            const auto &value_type = value.value().type();
            std::stringstream valuestream;
            if (value_type == typeid(std::string)) {
                valuestream << value.as<std::string>();
            } else if (value_type == typeid(int)) {
                valuestream << value.as<int>();
            } else if (value_type == typeid(bool)) {
               valuestream << std::boolalpha << value.as<bool>();
            } else if (value_type == typeid(std::vector<uint16_t>)) {
                auto list = value.as<std::vector<uint16_t>>();
                std::copy(list.begin(), list.end()
                    , std::ostream_iterator<uint16_t>(valuestream, " "));
            } else {
               valuestream << "(unknown type)";
            }
            BOOST_LOG_TRIVIAL(info) << name << ": " << valuestream.str();
        }
    }
}

void App::DecryptionThread() {
    BOOST_LOG_TRIVIAL(info) << "decryption thread started";
    Tins::NetworkInterface interface;
    for(const auto &nic : Tins::NetworkInterface::all()) {
      BOOST_LOG_TRIVIAL(info) << "available interfaces: " << nic.name();
    } 
    try {
     if(!interface_.empty()) {
       interface = Tins::NetworkInterface(interface_);
     } else {
       interface = Tins::NetworkInterface::default_interface();
     }
    } catch(const std::exception &e) {
      BOOST_LOG_TRIVIAL(error) << "unable to find interface: " << interface_;
      // TODO terminate program (via signal?)
    } 
    BOOST_LOG_TRIVIAL(info) << "chosen interface: " << interface.name();
    Tins::SnifferConfiguration conf;
    conf.set_promisc_mode(true);
    conf.set_filter("port 8080 or port 443");
    Tins::Sniffer sniffer(interface.name(), conf);
    //Decryptor decryptor(outputfile_, master_secrets_);
    PacketStreamFollower psf(&sniffer, master_secrets_.get(), outputfile_);
    //currently no shutdown mechanism implemented
    //this code should not be reached
    std::exit(0);
    BOOST_LOG_TRIVIAL(info) << "decryption thread ended";
}

void App::MainThread() {
    boost::thread mitm_thread{std::bind(&App::MitMThread, this)};
    boost::thread decryption_thread{std::bind(&App::DecryptionThread, this)};
    // blocking the main thread until SIGINT occurs:
    //  - first SIGINT waiting for network_io_service to run out of work
    //  - second SIGINT stop network_io_service and run out of work as well
    signal_ios_.run();
    // both io_services should terminate at the same time since the mutually
    // stop each other if they run out of work
    mitm_thread.join();
    //TODO terminate decryption thread properly
    //decryption_thread.join();
}

void App::MitMThread() {
    BOOST_LOG_TRIVIAL(info) << "mitm thread started";
    // starting the event-loop (server acceptor and sessions)
    network_ios_->run();
    // if server was shutdown and all sessions are terminated then stop the
    // signal io_service
    signal_ios_.stop();
    BOOST_LOG_TRIVIAL(info) << "mitm thread ended";
}

void App::ParseConfig(int argc, char **argv) {
    po::store(po::parse_command_line(argc, argv, description_), config_);
    po::notify(config_);
    if (!configfile_.empty()) {
        std::ifstream config_ifs{configfile_};
        if (config_ifs.is_open()) {
            po::store(po::parse_config_file(config_ifs, description_), config_);
            config_ifs.close();
        } else {
            BOOST_LOG_TRIVIAL(info) << "could not open config file: "
                << configfile_;
        }
        po::notify(config_);
    }
}

void App::PrintUsage() {
    std::cerr << "USAGE: " << program_name_ << " [OPTIONS...]" << std::endl;
    std::cerr << description_ << std::endl;
}

int App::Run(int argc, char** argv) {
    StoreProgramName(argv);
    InitDescription();
    ParseConfig(argc, argv);
    // print usage string and terminate if program was run with the help-flag
    if (help_) {
        PrintUsage();
        return EXIT_FAILURE;
    }
    LogConfig();
    InitLogging();
    InitServer();
    InitSigINT();
    MainThread();
    return EXIT_SUCCESS;
}

void App::StoreProgramName(char ** argv) {
    program_name_ = {argv[0]};
}
}  // namespace mp

