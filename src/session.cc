/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "session.h"

#include <algorithm>
#include <cstdint>
#include <iterator>
#include <map>
#include <string>
#include <vector>
#include <utility>

#include <boost/bind.hpp>
#include <boost/log/trivial.hpp>
#include <botan/botan.h>
#include <botan/hex.h>

#include <openssl/ssl.h>

#include "certificate_generator.h"
#include "nat.h"
#include "security_parameters.h"
#include "x509.h"

namespace mp {

using boost::asio::ip::tcp;

Session::Session(tcp::socket client_socket, tcp::socket server_socket
    , const bool insecure
    , const std::shared_ptr<CertificateGenerator> &certificate_generator
    , const std::shared_ptr<MasterSecrets> &master_secrets
    , const std::shared_ptr<std::map<uint16_t, bool>> &ssl_ports)
    : downstream_(std::move(client_socket))
    , upstream_(std::move(server_socket))
    , insecure_(insecure)
    , certificate_generator_(certificate_generator)
    , master_secrets_(master_secrets)
    , ssl_ports_(ssl_ports) {
    BOOST_LOG_TRIVIAL(info) << "new session instantiated";
}

Session::~Session() {
    BOOST_LOG_TRIVIAL(info) << "session terminated";
}

void Session::ConfigureServerHandshakeVerification() {
    boost::system::error_code error;
    if (!SSL_set_tlsext_host_name(upstream_.ssl.native_handle()
        , servername_.c_str())) {
        BOOST_LOG_TRIVIAL(error) << "error setting SNI servername";
    }
    BOOST_LOG_TRIVIAL(info) << "setting SNI servername to " << servername_;
    if (upstream_.context.set_default_verify_paths(error)) {
        BOOST_LOG_TRIVIAL(error) << error.message();
    }
    if(insecure_) {
      upstream_.ssl.set_verify_mode(boost::asio::ssl::verify_none, error);
    } else {
      upstream_.ssl.set_verify_mode(boost::asio::ssl::verify_peer
      | boost::asio::ssl::verify_fail_if_no_peer_cert, error);
      if (upstream_.ssl.set_verify_callback(
            boost::bind(&Session::VerifyCallback, this, _1, _2) , error)) 
        BOOST_LOG_TRIVIAL(error) << error.message();
      if (error) {
          BOOST_LOG_TRIVIAL(error) << "couldn't configure peer verification";
      } else {
          BOOST_LOG_TRIVIAL(info)
          << "server rfc2818_verification with servername: " << servername_
          << " configured";
      }
    }
}


void Session::ConfigureClientServernameCallback() {
    if (!SSL_CTX_set_tlsext_servername_callback(
        downstream_.context.native_handle()
            , &Session::ServerNameCallback)) {
        auto error = ERR_error_string(ERR_get_error(), nullptr);
        BOOST_LOG_TRIVIAL(error) << error;
    }
    if (!SSL_CTX_set_tlsext_servername_arg(
        downstream_.context.native_handle(), this)) {
        auto error = ERR_error_string(ERR_get_error(), nullptr);
        BOOST_LOG_TRIVIAL(error) << error;
    }
}


void Session::ConfigureCertificate(SSL* ssl) {
    if (!certificate_generator_) {
        BOOST_LOG_TRIVIAL(error) << "no certificate generator configured";
        return;
    }
    X509 cert = certificate_generator_->GetCertificate(servername_);
    if (SSL_use_certificate(ssl, cert.NativeHandle()) <= 0) {
        auto error = ERR_error_string(ERR_get_error(), nullptr);
        BOOST_LOG_TRIVIAL(error) << error;
    }
}

void Session::ConfigureKey(SSL* ssl) {
    if (!certificate_generator_) {
        BOOST_LOG_TRIVIAL(error) << "no certificate generator configured";
        return;
    }
    EVP_PKEY privateKey = certificate_generator_->GetKey();
    if (SSL_use_PrivateKey(ssl, privateKey.NativeHandle()) <= 0) {
        auto error = ERR_error_string(ERR_get_error(), nullptr);
        BOOST_LOG_TRIVIAL(error) << error;
    }
}

bool Session::IsSSL() {
    uint16_t protocol_port = original_destination_.port();
    bool port_found{};
    if (ssl_ports_) {
        if (ssl_ports_->count(protocol_port)) {
            port_found = true;
            if ((*ssl_ports_)[protocol_port]) {
                ssl_ = true;
                BOOST_LOG_TRIVIAL(debug) << "ssl port detected";
            } else {
                ssl_ = false;
                BOOST_LOG_TRIVIAL(debug) << "non-ssl port detected";
            }
        }
    }
    if (!port_found) {
        ssl_ = true;
        // check ClientHello
        if (client_hello_ == client_hello::sslv30) {
            BOOST_LOG_TRIVIAL(debug) << "SSLv3";
        } else if (client_hello_ == client_hello::tlsv10 ||
            client_hello_ == client_hello::tlsv11 ||
            client_hello_ == client_hello::tlsv12) {
            BOOST_LOG_TRIVIAL(debug) << "TLSv1.x";
        } else {
            ssl_ = false;
        }
    }
    BOOST_LOG_TRIVIAL(info) << "ssl mode: " << ssl_;
    return ssl_;
}

void Session::Start() {
    boost::system::error_code error;
    auto options = boost::asio::ssl::context::no_sslv2
        | boost::asio::ssl::context::no_sslv3;
    downstream_.context.set_options(options, error);
    SSL_CTX* ctx = downstream_.context.impl();
    if (!SSL_CTX_set_cipher_list(ctx, "AES256+SHA")) {
      throw std::runtime_error("SSL_CTX_set_cipher_list failed");
    } 
    upstream_.context.set_options(options, error);
    ctx = upstream_.context.impl();
    if (!SSL_CTX_set_cipher_list(ctx, "AES256+SHA")) {
      throw std::runtime_error("SSL_CTX_set_cipher_list failed");
    } 
    if (error) {
        BOOST_LOG_TRIVIAL(error) << "could not configure ssl context";
    } else {
        BOOST_LOG_TRIVIAL(info) << "configured client ssl context";
    }

    original_source_ = downstream_.socket.remote_endpoint(error);
    original_destination_ = nat::Resolve(&downstream_.socket);
    ConfigureClientServernameCallback();
    BOOST_LOG_TRIVIAL(info) << "new incoming connection from: "
        << original_source_ << " to remote server: "
        << original_destination_;
    if (error) {
        BOOST_LOG_TRIVIAL(error) << error.message();
        return;
    }
    boost::asio::async_read(downstream_.socket
        , boost::asio::buffer(client_hello_, client_hello_.size())
        , boost::bind(&Session::ReadInitalBytes, shared_from_this()
            , boost::asio::placeholders::error));
}

void Session::ClientHandleHandshake(boost::system::error_code error) {
    SSL *ssl = downstream_.ssl.native_handle(); 
    SSL_SESSION *session = SSL_get_session(ssl);
    auto cipher =  ::SSL_get_current_cipher(ssl); 
    if(cipher) {
      BOOST_LOG_TRIVIAL(info) << "cipher name: " << SSL_CIPHER_get_name(cipher) << std::endl;
      Botan::secure_vector<Botan::byte> master_key(SSL_MAX_MASTER_KEY_LENGTH);
      SSL_SESSION_get_master_key(session, master_key.data()
          , master_key.size());
      BOOST_LOG_TRIVIAL(info) << "master_key(" << master_key.size() << ")"
        << Botan::hex_encode(master_key);
      master_secrets_->Add(original_source_.address().to_string()
          ,original_source_.port(), master_key); 
    }
    if (!error) {
        BOOST_LOG_TRIVIAL(info) << "handshake with client successful";
        //FIXME handle if connection not possible
        //throws boost::system::system_error on failure 
        upstream_.socket.connect(original_destination_, error);
        if(error) {
          BOOST_LOG_TRIVIAL(error) << "could not connect to upstream server: "
            << error.message();
          ShutDown();
        }
        ConfigureServerHandshakeVerification();
        upstream_.ssl.async_handshake(boost::asio::ssl::stream_base::client
            , boost::bind(&Session::ServerHandleHandshake, shared_from_this()
            , boost::asio::placeholders::error));
    } else {
        BOOST_LOG_TRIVIAL(error) << error.message();
    }
}

void Session::ServerHandleHandshake(const boost::system::error_code &error) {
    SSL *ssl = upstream_.ssl.native_handle(); 
    SSL_SESSION *session = SSL_get_session(ssl);
    auto cipher =  SSL_get_current_cipher(ssl); 
    if(cipher) {
      BOOST_LOG_TRIVIAL(info) << "cipher name: " << SSL_CIPHER_get_name(cipher) << std::endl;
      Botan::secure_vector<Botan::byte> master_key(SSL_MAX_MASTER_KEY_LENGTH);
      SSL_SESSION_get_master_key(session, master_key.data()
          , master_key.size());
      BOOST_LOG_TRIVIAL(info) << "master_key(" << master_key.size() << ")"
        << Botan::hex_encode(master_key);
      //FIXME: add up and downstream to source address 
      const auto& socket = upstream_.socket;
      const auto& upstream_local = socket.local_endpoint();
      master_secrets_->Add(upstream_local.address().to_string()
          ,upstream_local.port(), master_key); 
    }
    if (!error) {
        BOOST_LOG_TRIVIAL(info) << "handshake with server successful";
        StartBridging();
    } else {
        BOOST_LOG_TRIVIAL(error) << error.message();
    }
}

void Session::Read(SessionEndpoint *endpoint, SessionEndpoint *otherEndpoint) {
    if (ssl_) {
        ReadFromStream(&endpoint->ssl, endpoint, otherEndpoint);
    } else {
        ReadFromStream(&endpoint->socket, endpoint, otherEndpoint);
    }
}

template<class T>
void Session::WriteToStream(T *stream, SessionEndpoint *endpoint
    , SessionEndpoint *otherEndpoint, size_t length) {
    auto self = shared_from_this();
    boost::asio::async_write(*stream
        , boost::asio::buffer(otherEndpoint->buffer, length)
        , [this, self, endpoint, otherEndpoint](boost::system::error_code error
            , std::size_t) {
        if (!error) {
            Read(otherEndpoint, endpoint);
        } else if (error == boost::asio::error::eof) {
            //ShutDown();
        } else {
            BOOST_LOG_TRIVIAL(error) << error.message();
        }
    });
}

void Session::ReadInitalBytes(boost::system::error_code error) {
    if (!error) {
        if (IsSSL()) {
            downstream_.ssl.async_handshake(
                  boost::asio::ssl::stream_base::server
                , boost::asio::buffer(client_hello_, client_hello_.size())
                , boost::bind(&Session::ClientHandleHandshake
                    , shared_from_this()
                , boost::asio::placeholders::error));
        } else {
            //TODO catch exception
            upstream_.socket.connect(original_destination_);
            auto self = shared_from_this();
            boost::asio::async_write(upstream_.socket
                , boost::asio::buffer(client_hello_, client_hello_.size())
                , [this, self](boost::system::error_code, size_t) {
                    StartBridging();
                });
        }
    }
}

template<class T>
void Session::ReadFromStream(T *stream, SessionEndpoint *endpoint
    , SessionEndpoint *otherEndpoint) {
    auto self = shared_from_this();
    auto &buffer = endpoint->buffer;
    boost::asio::async_read(*stream, boost::asio::buffer(buffer, buffer.size())
        , boost::asio::transfer_at_least(1)
        , [this, self, endpoint, otherEndpoint](boost::system::error_code error
            , std::size_t length) {
        if (!error) {
        //TODO remove unnecessary code
            //if (length > 0) {
            //    auto it = endpoint->buffer.begin();
            //    std::string data{it, it + length};
            //}
            Write(otherEndpoint, endpoint, length);
        } else if (error == boost::asio::error::eof) {
            ShutDown();
        } else if (error.message() != "short read") {
            BOOST_LOG_TRIVIAL(error) << error.message();
        }
    });
}

void Session::StartBridging() {
    Read(&downstream_, &upstream_);
    Read(&upstream_, &downstream_);
}

void Session::Write(SessionEndpoint *endpoint, SessionEndpoint *otherEndpoint
    , std::size_t length) {
    auto self = shared_from_this();
    if (ssl_) {
        WriteToStream(&(endpoint->ssl), endpoint, otherEndpoint, length);
    } else {
        WriteToStream(&(endpoint->socket), endpoint, otherEndpoint, length);
    }
}

void Session::ShutDown() {
    struct SessionEndpointName {
        SessionEndpoint &endpoint;
        std::string name;
    };
    auto endpoint_name_pairs = std::vector<SessionEndpointName>{
        {downstream_, "downstream"}, {upstream_, "upstream"}};
    for (const auto &e : endpoint_name_pairs) {
        auto &socket = e.endpoint.socket;
        boost::system::error_code error;
        socket.shutdown(tcp::socket::shutdown_both, error);
        if (!error || error == boost::asio::error::not_connected) {
            // socket was not connected
        } else {
            BOOST_LOG_TRIVIAL(error)
                << "error while shutting down socket '" << e.name << "': "
                << error.message();
        }
    }
}

int Session::ServerNameCallback(SSL *ssl, int * /* ad */ , void *arg) {
    Session *self = static_cast<Session *>(arg);
    if (ssl == NULL) {
        return SSL_TLSEXT_ERR_NOACK;
    }
    if (SSL_get_servername(ssl, TLSEXT_NAMETYPE_host_name)) {
        self->servername_ = std::string(SSL_get_servername(ssl
            , TLSEXT_NAMETYPE_host_name));
        BOOST_LOG_TRIVIAL(info) << "tlsext_host_name: " << self->servername_;
    } else {
      self->servername_ = self->original_destination_.address().to_string();
    }
    self->ConfigureKey(ssl);
    self->ConfigureCertificate(ssl);
    return SSL_TLSEXT_ERR_OK;
}

bool Session::VerifyCallback(bool preverified
    , boost::asio::ssl::verify_context &ctx) {
    auto rfc2818 = boost::asio::ssl::rfc2818_verification(servername_);
    char subject_name[256];
    ::X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
    X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);
    BOOST_LOG_TRIVIAL(debug) << "Verifying " << subject_name;
    return rfc2818(preverified, ctx);
}
}  // namespace mp
