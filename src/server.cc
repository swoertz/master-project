/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "server.h"

#include <boost/log/trivial.hpp>

#include "session.h"

namespace mp {

using boost::asio::ip::tcp;

Server::Server(const std::shared_ptr<boost::asio::io_service> &io_service
  , const unsigned short port
  , const bool insecure
  , const bool transparent
  , const std::shared_ptr<CertificateGenerator> &certificate_generator
  , const std::shared_ptr<MasterSecrets> &master_secrets
  , const std::vector<uint16_t> &ssl_ports
  , const std::vector<uint16_t> &non_ssl_ports)
    : acceptor_(*io_service)
    , client_(*io_service)
    , server_(*io_service)
    , insecure_(insecure)
    , io_service_(io_service)
    , certificate_generator_(certificate_generator)
    , master_secrets_(master_secrets) 
    , ssl_ports_{std::make_shared<std::map<uint16_t, bool>>()} {
  for (auto p : ssl_ports) {
    ssl_ports_->insert(std::make_pair(p, true));
  }
  for (auto p : non_ssl_ports) {
    ssl_ports_->insert(std::make_pair(p, false));
  }
  if (InitAcceptor(port, transparent)) {
    Loop();
  }
}

void Server::Loop() {
  client_ = tcp::socket(*io_service_);
  server_ = tcp::socket(*io_service_);
  acceptor_.async_accept(client_,
    [this](boost::system::error_code error) {
      switch (error.value()) {
        case boost::system::errc::success: {
          auto session = std::make_shared<Session>(std::move(client_)
            , std::move(server_), insecure_, certificate_generator_
            , master_secrets_, ssl_ports_);
          session->Start();
          Loop();
          break;
        }
        case boost::system::errc::operation_canceled: {
          break;
        }
        default: {
          BOOST_LOG_TRIVIAL(error) << error.message();
        }
      }
    });
}

bool Server::InitAcceptor(unsigned short port, bool transparent) {
  //TODO ipv6
  auto endpoint = tcp::endpoint{tcp::v4(), port};
  acceptor_.open(endpoint.protocol());
  boost::system::error_code error;
  acceptor_.bind(endpoint, error);
  if (error) {
    BOOST_LOG_TRIVIAL(error) << "could not bind acceptor to: "
      << endpoint;
    return false;
  }
  BOOST_LOG_TRIVIAL(info) << "bind acceptor to: " << endpoint;
  if (transparent) {
    SetupTransparentMode();
  }
  acceptor_.listen();
  return true;
}

void Server::SetupTransparentMode() {
  boost::asio::detail::socket_option::boolean<SOL_IP, IP_TRANSPARENT>
  transparent_opt{true};
  auto error = boost::system::error_code{};
  acceptor_.set_option(transparent_opt, error);
  if (error.value() == boost::system::errc::success) {
    BOOST_LOG_TRIVIAL(info) << "set the socket option IP_TRANSPARENT";
  } else {
    BOOST_LOG_TRIVIAL(error) << error.message();
  }
}

void Server::Shutdown() {
  BOOST_LOG_TRIVIAL(info) << "closing acceptor socket";
  acceptor_.close();
}
}  // namespace mp
