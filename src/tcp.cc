/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tcp.h"

namespace mp {
std::ostream& operator<<(std::ostream &stream, const Tins::TCP &tcp) {
  stream << "TCP ";
  if(tcp.flags() & Tins::TCP::Flags::FIN) {
    stream << "FIN ";
  }
  if(tcp.flags() & Tins::TCP::Flags::SYN) {
    stream << "SYN ";
  }
  if(tcp.flags() & Tins::TCP::Flags::RST) {
    stream << "RST ";
  }
  if(tcp.flags() & Tins::TCP::Flags::ACK) {
    stream << "ACK ";
  }
  return stream;
}
}  // namespace mp

