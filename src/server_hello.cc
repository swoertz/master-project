/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "server_hello.h"

namespace mp {
ServerHello::ServerHello(std::vector<uint8_t> &data) : Handshake(data) {
      std::copy_n(Handshake::body_, sizeof(ServerHelloData)
          , reinterpret_cast<uint8_t*>(&data_));
 // std::cout << "body_: " << std::hex << (int) *(body_+5)<< (int) *(body_+4)<< (int) *(body_+3) << (int) *(body_+2)<< std::endl;
 // std::cout << "data: " << std::hex << (int)data[14] << (int)data[13] << (int)data[12]  << (int)data[11] << std::endl;
 // std::cout << "data_: " << std::hex << data_.random.gmt_unix_time << std::endl;
} 

std::array<uint8_t, sizeof(Random)> ServerHello::random() {
  std::array<uint8_t, sizeof(Random)> ret;
  auto it = reinterpret_cast<uint8_t*>(&data_.random);
  std::copy(it, it + sizeof(Random),ret.begin());
  return ret;
}
    
std::array<uint8_t, sizeof(SessionId)> ServerHello::session_id() {
  std::array<uint8_t, sizeof(SessionId)> ret;
  auto it = reinterpret_cast<uint8_t*>(&data_.session_id);
  std::copy(it, it + sizeof(SessionId),ret.begin());
  return ret;
}

void ServerHello::SetRandom(SecurityParameters *params) {
  auto rnd = random();
  std::copy(rnd.begin(), rnd.end(), params->server_random.begin());
}
}  // namespace mp

