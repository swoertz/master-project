/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "client_hello.h" 

namespace mp {
ClientHello::ClientHello(std::vector<uint8_t> &data) : Handshake(data) {
  std::copy_n(Handshake::body_, sizeof(ClientHelloData)
      , reinterpret_cast<uint8_t*>(&data_));
  //TODO remove
  //std::cout << "data: " << std::hex << (int)data[14] << (int)data[13] << (int)data[12]  << (int)data[11] << std::endl;
  //std::cout << "random: " << std::hex << data_.random.gmt_unix_time << std::endl;
} 

std::array<uint8_t, 32> ClientHello::random() {
  std::array<uint8_t,32> ret;
  //TODO refactor with server_hello 
  auto it = reinterpret_cast<uint8_t*>(&data_.random);
  std::copy_n(it, 32, ret.begin());
  return ret;
}
void ClientHello::SetRandom(SecurityParameters *params) {
  auto rnd = random();
  std::copy(rnd.begin(), rnd.end(), params->client_random.begin());
}
}  // namespace mp
