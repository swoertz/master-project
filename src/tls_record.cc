/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tls_record.h"

#include <algorithm>
#include <sstream>

#include <tins/tins.h>

#include "handshake.h"

namespace mp {
TlsRecord::TlsRecord(std::vector<uint8_t> &data) {
  if(data.size() < sizeof(TlsRecordHeader)) {
    throw Tins::malformed_packet();
  }
  static_assert(sizeof(TlsRecordHeader) == 5
      , "the size of the header structure has to be 5 bytes");
  body_ = std::next(data.begin(), sizeof(TlsRecordHeader));
  std::copy(data.begin(), body_, reinterpret_cast<uint8_t*>(&header_));
  auto distance = std::distance(body_, data.end());
  if(distance < length()) {
    throw Tins::malformed_packet();
  }
}

std::string TlsRecord::Serialize() const {
  std::stringstream stream;
  stream << protocol_version();
  switch(content_type()) {
   case APPLICATION_DATA:
     break;
   case ALERT:
     stream << " Alert";
     break;
   case HANDSHAKE:
     break;
   case CHANGE_CIPHER_SPEC:
     stream << " Change cipher spec";
     break;
   default:
     stream << static_cast<int>(content_type());
     break;
  }  
  return stream.str();
}

std::ostream& operator<<(std::ostream& stream, const TlsRecord &record) {
  return stream << record.Serialize();
}
}  // namespace mp

