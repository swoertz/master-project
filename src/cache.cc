/*
 * Copyright 2017 Simon Woertz
 *
 * This file is part of master-project.
 *
 *   master-project is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   master-project is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with master-project.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cache.h"

namespace mp {

Cache::value_t Cache::Get(const key_t &key) {
  auto it = map_.find(key); 
  value_t value;
  if(it != map_.end()) {
    auto key_value_it = it->second;
    value = key_value_it->second;
    // TODO put key_value_t to front of list
    list_.splice(list_.begin(), list_, key_value_it);
  } else {
    // TODO generate value
  }
  return value;
} 

void Cache::Put(key_t key, value_t value){
  list_.push_front(make_pair(key, value));
  map_.emplace(key, list_.begin());

 // TODO if max size (delete last in list) 
}

}  // namespace mp

