#!/bin/bash

echo "dropping the iptables rules in the tables nat and mangle"
iptables -t nat -F
iptables -t mangle -F

echo "enabling masquerade for the internal network (NAT)"
iptables -t nat -A POSTROUTING -s 192.168.10.0/24 -j MASQUERADE

echo "enable tproxy mode according to https://www.kernel.org/doc/Documentation/networking/tproxy.txt"
# new chain DIVERT
iptables -t mangle -N DIVERT
# put tcp traffic into DIVERT chain
iptables -t mangle -A PREROUTING -p tcp -m socket -j DIVERT
# jumpt to mark
iptables -t mangle -A DIVERT -j MARK --set-mark 1
# jump to accept
iptables -t mangle -A DIVERT -j ACCEPT

iptables -t mangle -A PREROUTING -p tcp --dport 443 -j TPROXY --tproxy-mark 0x1/0x1 --on-port 8080

ip rule add fwmark 1 lookup 100
ip route add local 0.0.0.0/0 dev lo table 100

echo "#################### LIST NAT TABLE ####################"
iptables -t nat -L
echo "################## LIST MANGLE TABLE ###################"
iptables -t mangle -L
